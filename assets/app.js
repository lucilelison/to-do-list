
// >>>PASSER D'UN ONGLET A UN AUTRE<<<

function selView(n, litag) {
  var todoview = "none";
  var doneview = "none";
  var allview = "none";
  switch(n) {
    case 1:
        todoview = "inline";
        break;
    case 2:
        doneview = "inline";
        break;
    case 3:
	    allview = "inline";
	    break;
    
    default:
    	todoview = "inline";
      	break;
  }

  document.getElementById("tab_todo").style.display = todoview;
  document.getElementById("tab_done").style.display = doneview;
  document.getElementById("tab_all").style.display = allview;
  var tabs = document.getElementById("tabs");
  var ca = Array.prototype.slice.call(tabs.querySelectorAll("li"));
  ca.map(function(elem) {
    elem.style.background="#F0F0F0";
    elem.style.borderBottom="1px solid gray"
  });

  litag.style.borderBottom = "1px solid white";
  litag.style.background = "white";
}


function selInit() {
  var tabs = document.getElementById("tabs");
  var litag = tabs.querySelector("tab_all");   // first li
  litag.style.borderBottom = "1px solid white";
  litag.style.background = "white";
}


// >>>BLOC ALL / BTN_NEW PROMPT CREA LISTE ToDo<<<

var btn_new = document.getElementById('btn_new');

btn_new.addEventListener("click", function() {
	
	var nouvelle_tache = prompt ("nouvelle tache");
	var checkbox = document.createElement("input");
	var li = document.createElement("li");
	var children = ul_all.children.length + 1;

	checkbox.setAttribute('type', 'checkbox');
	checkbox.setAttribute('id', nouvelle_tache + "checkbox_id" + children);
	checkbox.className = "checkcss";
    
    li.setAttribute('id', 'tache'+children);
    li.setAttribute('class', 'li_element')
    li.innerHTML = nouvelle_tache;
	li.appendChild(checkbox);
	
	document.getElementById('ul_all').appendChild(li);

	var check = document.getElementById("tache"+children)

	check.addEventListener("click", function(){
		if (document.getElementById(nouvelle_tache + "checkbox_id"+children).checked) {
		document.getElementById('tache'+children).style.textDecoration = "line-through"
		}
		else document.getElementById('tache' + children).style.textDecoration = "none"

	})
	
})


var btn_done = document.getElementById('btn_done')
//var box_checked = document.getElementById('tache' + children).checked
var btn_todo = document.getElementById('btn_todo')
		
btn_done.addEventListener("click", function(){
		let elements = document.getElementsByClassName("li_element")
		//console.log(elements)

		for (element of elements) {
			if (element.style.textDecoration == "line-through"){
				document.getElementById("bloc_done").append(element)
			}
		

		}
})

btn_todo.addEventListener("click", function(){
		let elements = document.getElementsByClassName("li_element")
		for (element of elements) {
			if (element.style.textDecoration == "line-through"){
				document.getElementById("bloc_todo").append(element)
			}
		}
})